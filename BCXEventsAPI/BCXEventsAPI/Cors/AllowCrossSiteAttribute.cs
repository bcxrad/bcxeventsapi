﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BCXEventsAPI.Cors
{
    public class AllowCrossSiteAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            context.HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "*");
            context.HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            context.HttpContext.Response.Headers.Add("Vary", "Origin");
            context.HttpContext.Response.Headers.Add("Cache-Control", "no-cache");
            context.HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST");
            base.OnActionExecuting(context);
        }
    }
}
