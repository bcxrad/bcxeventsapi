﻿using System;
using System.Collections.Generic;
using BCXEventsAPI.Models;

namespace BCXEventsAPI
{
    public class EventList
    {
        public int responseCode { get; set; }
        public string response { get; set; }
        public string transactionId { get; set; }
        public List<EventEntry> eventEntries { get; set; }
    }
}
