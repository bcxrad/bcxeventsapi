﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BCXEventsAPI.Adapters;
using BCXEventsAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BCXEventsAPI.Controllers
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {

        private const String SELECT_EVENT_ENTRIES = "SELECT * FROM EVENTS ORDER BY EVENT_DATE DESC";
        private const String SELECT_SINGLE_EVENT = "SELECT * FROM EVENTS WHERE ID = @id";
        private const String INSERT_EVENT_ENTRY = "INSERT INTO EVENTS (EVENT_IMAGE_URL, EVENT_TITLE, EVENT_DESCRIPTION, " +
                                                    "EVENT_ADDRESS, EVENT_DATE) VALUES (@imageUrl,@title,@description,@address,@date)";
        private const String BLOB_STORAGE_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";
        // GET api/values
        [HttpGet]
        public EventList Get()
        {
            EventList eventList = new EventList();
            List<EventEntry> eventEntries = new List<EventEntry>();
            Database db = new Database();
            try
            {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(SELECT_EVENT_ENTRIES);
                SqlDataReader myReader = null;
                myReader = sqlComm.ExecuteReader();
                int count = 1;
                while (myReader.Read())
                {
                    EventEntry eventEntry = new EventEntry();
                    eventEntry.imageUrl = BLOB_STORAGE_URL + myReader["event_image_url"].ToString();
                    eventEntry.title = myReader["event_title"].ToString();
                    eventEntry.description = myReader["event_description"].ToString();
                    eventEntry.address = myReader["event_address"].ToString();
                    eventEntry.eventDate = myReader["event_date"].ToString();
                    eventEntries.Add(eventEntry);
                    count++;
                }
                eventList.transactionId = "oie928hr4oubf4";
                eventList.eventEntries = eventEntries;
            }
            catch (Exception e)
            {
                eventList.responseCode = 500;
                eventList.response = e.ToString();
            }
            finally
            {
                db.Close();
            }
            return eventList;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]JObject eventEntry)
        {
            Database db = new Database();
            try
            {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_EVENT_ENTRY);
                sqlComm.Parameters.AddWithValue("@imageUrl", (string)eventEntry.GetValue("imageUrl"));
                sqlComm.Parameters.AddWithValue("@title", (string)eventEntry.GetValue("title"));
                sqlComm.Parameters.AddWithValue("@description", (string)eventEntry.GetValue("description"));
                sqlComm.Parameters.AddWithValue("@address", (string)eventEntry.GetValue("address"));
                sqlComm.Parameters.AddWithValue("@date", (string)eventEntry.GetValue("date"));
                sqlComm.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
